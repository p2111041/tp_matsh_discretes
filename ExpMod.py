def ExpMod(a,k,n):
    print("L'exponentiation modulaire de ",a,"puissance ", k ,"mod " ,n ," est égale à : ")
    p = 1
    while k>0:
        if k>0:
            if(k%2 != 0):
                p = (p*a)%n
            a = (a*a)%n     
            k = k//2
    return p

#-------------------------------#
a = 11
k = 13
n = 19

result = ExpMod(a,k,n)
print(result)