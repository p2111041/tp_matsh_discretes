def A1( n1,n2,e1,e2,M1,M2) : 
    m = 0
    n=min(n1,n2)
    for i in range (1,n-1) :
        if (euclide(i,n1)==1) and (euclide(i,n2)==1) and ExpMod(i,e1,n1)==M1 and ExpMod(i,e2,n2)==M2:
                m = i
                break    
    return m

def A2( n1,e1,M1) : 
    m = 0
    for i in range (1,n1-1) :
        if (euclide(i,n1)==1) and ExpMod(i,e1,n1)==M1:
                m = i
                break    
    return m

def A3( n1,e1,e2,M1,M2) : 
    m = 0
    for i in range (1,n1-1) :
        if (euclide(i,n1)==1) and ExpMod(i,e1,n1)==M1 and ExpMod(i,e2,n1)==M2:
                m = i
                break    
    return m

def A4( n1,n2,n3,M1,M2,M3) : 
    m = 0
    n = min(n1,n2,n3)
    for i in range (1,n-1) :
        if (euclide(i,n1)==1) and ExpMod(i,3,n2)==M2 and (euclide(i,n2)==1) and euclide(i,n3)==1:
            if(ExpMod(i,3,n1)==M1 and ExpMod(i,3,n2)==M2 and ExpMod(i,3,n3)==M3):
                m = i
                break    
    return m




def euclide(a, b):
    while b != 0 :
        t = b; 
        b = a % b; 
        a = t; 
    return a


def phi(n) : 
    tab = []
    for i in range(2,n):
        if euclide(i,n) == 1 : 
            tab.append(i)
    return len(tab)



def EuclideEtendu(a,b):
    if b==0 :
        return (a,1,0)
    (dp,xp,yp) = EuclideEtendu(b,a%b)
    d=dp
    x=yp
    y=xp-int(a/b)*yp
    return (d,x,y) 



def ExpMod(a,k,n):
    #print("L'exponentiation modulaire de ",a,"puissance ", k ,"mod " ,n ," est égale à : ")
    p = 1
    while k>0:
        if k>0:
            if(k%2 != 0):
                p = (p*a)%n
            a = (a*a)%n     
            k = k//2
    return p
 
print ("Pour n1 = 21, n2 = 21, e1 = 3, e2 = 3, M1 = 8, M2 = 8, m =" ,A2(21,3,8),'avec l algorithme A1')