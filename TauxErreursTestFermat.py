import time

def ExpMod(a,k,n):
    p = 1
    while k>0:
        if k>0:
            if(k%2 != 0):
                p = (p*a)%n
            a = (a*a)%n     
            k = k//2
    return p

def TestFermatVite(n):
    return ExpMod(2,n-1,n) == 1

def Proportion():
    puiss2 = 2
    totalp = 0
    totalnp = 0
    tabnbrpremier = []
    tabproportion = []
    tabpremier = []
    startf = time.time()
    for i in range (1,31):
        start = time.time()
        p=0
        np=0
        tab=[]
        for j in range (puiss2,puiss2*2):
            if(TestFermatVite(j)):
                p+=1
                totalp +=1
            else:
                np+=1
                totalnp +=1
        proportion = p/(p+np)
        end = time.time()
        tabproportion.append(proportion)
        print("")
        print("Nombre de nombres premiers",p)
        tabnbrpremier.append(p)
        print("Liste du nombre de nombre premiers",tabnbrpremier)
        print("Nombre total de nombres premiers",totalp)
        print("Proportion sur l'ensemble", puiss2, "à", puiss2*2,"est",proportion)
        print("Liste des proportions",tabproportion)
        proportiontotal = totalp/(totalp+totalnp)
        print("Proportion totale jusqu'ici",proportiontotal)
        puiss2 = puiss2*2
        print("Temps de calcul pour cet ensemble",end-start)
        print("")
    endf = time.time()
    print("Temps de calcul final",endf - startf)
#------------------------------------------------#
