def TestFermat(n):
    if(n == 0 ):
        print("n n'est pas premier.")
        return
    res =1
    for i in range(1,n):
        res *=2
    res = res%n
    if(res == 1):
        print("n est premier.")
    else:
        print("n n'est pas premier.")


#-------------------#

print("Test de Fermat avec 0 :")
TestFermat(0)

print("Test de Fermat avec 13 : ")
TestFermat(13)

print("Test de Fermat avec 123342 : ")
TestFermat(123342)