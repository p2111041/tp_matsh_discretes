# – Soit n = pq un produit de 2 nombres premiers distincts. Proposer un algorithme polynomial
# PhiToFact tel que PhiToFact(n, ϕ(n)) retourne la factorisation de n. En d´eduire qu’il n’existe pas
# d’algorithme polynomial A tel que A(n) → ϕ(n) s’il n’existe pas d’algorithme polynomial de factorisation.

# Nombre de npremiers avec n donc pgcd des deux == 1 
from tkinter import N


def euclide(a, b):
    while b != 0 :
        t = b; 
        b = a % b; 
        a = t; 
    return a

def phi(n) : 
    tab = []
    for i in range(2,n):
        if euclide(i,n) == 1 : 
            tab.append(i)
    return len(tab)

def phitofact(n) : 
    print("Test de phitofact pour n=",n)
    tab = []
    tab = tabPremiers(n)
    for i in range(0, len(tab)): 
        for j in range(0, len(tab)): 
            if (tab[i]*tab[j] == n) or (phi(i) == tab[i]-1):
                p = tab[i]
                q = tab[j]
                if (phi(n)==(tab[i]-1)*(tab[j]-1)):
                    p = tab[i]
                    q = tab[j]
                    print ("On trouve phi(",n,") = (",p,"-1)*(",q,"-1)")           
                    return
    print("Il n'y a pas n = p*q tels que p et q soient des nombres premiers distincts")
    return


def tabPremiers(n) : 
    tab = []
    for i in range(n) :
        if PrimaliteNaif(i) == True :
            tab.append(i)
    return tab 


def PrimaliteNaif(n):
    if n<=1:
        #print("Le nombre n'est pas un entier supérieur à 2")
        return False
    for i in range(2,n):
        if n%i == 0:
            #print("Le nombre n'est pas premier")
            return False
    #print("Le nombre",n, "est premier")*
    return True   


phitofact(121)    