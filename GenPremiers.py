import random
import time

def Binaire(n):
    puiss2 = 1
    taille = 0
    result = 2
    while(n>puiss2):
        puiss2 *= 2
        taille += 1
        if(taille%2) == 0: result*=2
    return result

def PrimaliteNaif(n):
    if n<=1:
        print("Le nombre n'est pas un entier supérieur à 2")
        return False
    for i in range(2, Binaire(n)):
        if n%i == 0:
            return False
    return True

def ExpMod(a,k,n):
    p = 1
    while k>0:
        if k>0:
            if(k%2 != 0):
                p = (p*a)%n
            a = (a*a)%n     
            k = k//2
    return p

def TestFermat(n):
    return ExpMod(2,n-1,n) == 1


def GenPremiersNaif(k):#Version primalité naive qui joue su l'aléatoire qui peut être rapide comme très très lente
    p=2
    for i in range(1,k-1):
        p *=2
    rand = random.randint(p,2*p-1)
    while(rand%2==0 or not PrimaliteNaif(rand)):
        print("rand3",rand)
        rand = random.randint(p,2*p-1)
        print("cabugpo")
    print("Nombre premiers de ",k,"bits ",rand)
    return(rand)

def GenPremiersRegu(k):#Version primalité naive qui est lente mais on est sur que le résultat est bon
    p=2
    for i in range (1,k-1):
        p *=2
    rand = random.randint(p,2*p-1)
    while(rand%2==0):
        rand = random.randint(p,2*p-1)
    temp = rand
    for i in range (p,2*p-1):
        print(temp)
        if(TestFermat(temp)):
            if(PrimaliteNaif(temp)):
                print("Nombre premiers de ",k,"bits ",temp)
                return temp
        temp +=2
    print("Nombre premiers de ",k,"bits ",temp)
    return(temp)

def GenPremiersNaifFermat(k):#Version naive avec le test de Fermat qui peut être très très rapide ou relativement rapide 
                            #(si on tombe directement sur le bon nombre c'est instanné)
    p=2
    for i in range(1,k-1):
        p *=2
    rand = random.randint(p,2*p-1)
    while(rand%2==0 or not TestFermat(rand)):
        rand = random.randint(p,2*p-1)
    print("Nombre premiers de ",k,"bits ",rand)
    return(rand)

def GenPremiersReguFermat(k):#Version avec le test de Fermat rapide avec un temps tout le temps très court (en moyenne plus court)
    p=2
    for i in range (1,k-1):
        p *=2
    rand = random.randint(p,2*p-1)
    while(rand%2==0):
        rand = random.randint(p,2*p-1)
    temp = rand
    for i in range (p,2*p-1):
        if(TestFermat(temp)):
            print("Nombre premiers de ",k,"bits ",temp)
            return temp
        temp +=2
    print("Nombre premiers de ",k,"bits ",temp)
    return(temp)

#--------------
start = time.time()
GenPremiersNaifFermat(1000)
end = time.time()
print("Temps d'execution ",end-start)

start = time.time()
GenPremiersReguFermat(1000)
end = time.time()
print("Temps d'execution ",end-start)

