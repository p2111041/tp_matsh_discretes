import time

def ExpMod(a,k,n):
    p = 1
    while k>0:
        if k>0:
            if(k%2 != 0):
                p = (p*a)%n
            a = (a*a)%n     
            k = k/2
    return p

def Binaire(n):
    puiss2 = 1
    taille = 0
    result = 2
    if(n==3):
        return 3
    while(n>puiss2):
        puiss2 *= 2
        taille += 1
        if(taille%2) == 0: result*=2
    return result

def PrimaliteNaif(n):
    if n<=1:
        print("Le nombre n'est pas un entier supérieur à 2")
        return False
    for i in range(2,Binaire(n)):
        if n%i == 0:
            #print("Le nombre n'est pas premier")
            return False
    #print("Le nombre",n, "est premier")*
    return True   

def PrimaliteNaifTab(n,tab):
    if n<=1:
        print("Le nombre n'est pas un entier supérieur à 2")
        return False
    i = 0
    while(i<len(tab) and tab[i]<Binaire(n)):
        if n%tab[i] == 0:
            #print("Le nombre n'est pas premier")
            return False
        i+=1
    #print("Le nombre",n, "est premier")*
    tab.append(n)
    return True

def Proportion():
    puiss2 = 2
    totalp = 0
    totalnp = 0
    tabnbrpremier = []
    tabproportion = []
    tabpremier = []
    startf = time.time()
    for i in range (1,31):
        start = time.time()
        p=0
        np=0
        tab=[]
        for j in range (puiss2,puiss2*2):
            if(PrimaliteNaifTab(j, tabpremier)):
                p+=1
                totalp +=1
            else:
                np+=1
                totalnp +=1
        proportion = p/(p+np)
        end = time.time()
        tabproportion.append(proportion)
        print("")
        print("Nombre de nombres premiers",p)
        tabnbrpremier.append(p)
        print("Liste du nombre de nombre premiers",tabnbrpremier)
        print("Nombre total de nombres premiers",totalp)
        print("Proportion sur l'ensemble", puiss2, "à", puiss2*2,"est",proportion)
        print("Liste des proportions",tabproportion)
        proportiontotal = totalp/(totalp+totalnp)
        print("Proportion totale jusqu'ici",proportiontotal)
        puiss2 = puiss2*2
        print("Temps de calcul pour cet ensemble",end-start)
        print("")
    endf = time.time()
    print("Temps de calcul final",endf - startf)
#------------------------------------------------#
# Proportion()

#---------------------------------------------#
# print("Test de Primalité Naif")
# start = time.time()
# print(PrimaliteNaif(1527887))
# print(PrimaliteNaif(892307681963237))
# end = time.time()
# print("Temps de calcul final",end - start)

