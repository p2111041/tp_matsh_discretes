def EuclideEtendu(a,b):
    if b==0 :
        return (a,1,0)
    (dp,xp,yp) = EuclideEtendu(b,a%b)
    d=dp
    x=yp
    y=xp-int(a/b)*yp
    return (d,x,y)


#---------------------------------#

(a,b,c)=EuclideEtendu(243,198)
print("Le pgcd est :",a,", les coefficients de Bézout sont : ",b," et ",c)